%global nfsmountable 1

%global _scl_prefix /opt/isc

%global scl_name_prefix isc-
%global scl_name_base bind

%global scl %{scl_name_prefix}%{scl_name_base}

# Work around an SCL build issue on Fedora 33+
# (https://bugzilla.redhat.com/show_bug.cgi?id=1898587)
%if 0%{?fedora} >= 33
%global __python %{__python3}
%endif

%scl_package %scl

Summary: Package that installs %scl
Name: %scl_name
Epoch: 1
Version: 2
Release: 3%{?dist}
License: MPL 2.0
Requires: %{scl_prefix}bind
Requires: %{scl_prefix}bind-utils
%if 0%{?rhel} >= 8 || 0%{?fedora} >= 23
Requires: policycoreutils-python-utils
%else
Requires: policycoreutils-python
%endif
BuildRequires: scl-utils-build

%description
This is the main package for %scl Software Collection.

%package runtime
Summary: Package that handles %scl Software Collection.
Requires: scl-utils

%description runtime
Package shipping essential scripts to work with %scl Software Collection.

%package build
Summary: Package shipping basic build configuration
Requires: scl-utils-build

%description build
Package shipping essential configuration macros to build %scl Software Collection.

%prep
%setup -c -T

%install
%scl_install

cat >> %{buildroot}%{_root_sysconfdir}/rpm/macros.%{scl}-config <<EOF
%%_scl_prefix %{_scl_prefix}
EOF

cat > %{buildroot}%{_scl_scripts}/enable <<EOF
export PATH="%{_bindir}:%{_sbindir}\${PATH:+:\${PATH}}"
export LD_LIBRARY_PATH="%{_libdir}\${LD_LIBRARY_PATH:+:\${LD_LIBRARY_PATH}}"
export MANPATH="%{_mandir}\${MANPATH:+:\${MANPATH}}"
EOF

%files

%files runtime
%scl_files

%files build
%{_root_sysconfdir}/rpm/macros.%{scl}-config

%post
semanage fcontext -a -e / %{_scl_root} 2>/dev/null || :
semanage fcontext -a -e /etc %{_sysconfdir} 2>/dev/null || :
semanage fcontext -a -e /var %{_localstatedir} 2>/dev/null || :
restorecon -R %{_scl_root} %{_sysconfdir} %{_localstatedir} || :
selinuxenabled && load_policy || :

%postun
if [ "$1" -eq 0 ]; then
	# Package removal, not upgrade
	semanage fcontext -d -e / %{_scl_root} 2>/dev/null || :
	semanage fcontext -d -e /etc %{_sysconfdir} 2>/dev/null || :
	semanage fcontext -d -e /var %{_localstatedir} 2>/dev/null || :
fi
